object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Labirinto'
  ClientHeight = 489
  ClientWidth = 759
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 759
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 103
      Height = 13
      Caption = 'Quantidade de lados:'
    end
    object Label2: TLabel
      Left = 41
      Top = 30
      Width = 6
      Height = 13
      Caption = 'x'
    end
    object Edit1: TEdit
      Left = 8
      Top = 27
      Width = 25
      Height = 21
      TabOrder = 0
      Text = '10'
    end
    object Edit2: TEdit
      Left = 53
      Top = 27
      Width = 24
      Height = 21
      TabOrder = 1
      Text = '10'
    end
    object Button1: TButton
      Left = 83
      Top = 26
      Width = 134
      Height = 25
      Caption = 'Gerar Labirinto'
      TabOrder = 2
      OnClick = Button1Click
    end
    object rdgAlgoritmo: TRadioGroup
      Left = 240
      Top = 8
      Width = 409
      Height = 43
      Caption = 'Algor'#237'tmos de cria'#231#227'o de labirinto:'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Binary Tree'
        'Sidewinder'
        'Recursive Backtracking')
      TabOrder = 3
    end
    object BitBtn1: TBitBtn
      Left = 655
      Top = 26
      Width = 98
      Height = 25
      Caption = 'Imprimir'
      TabOrder = 4
      OnClick = BitBtn1Click
    end
  end
  object ScrollBox1: TScrollBox
    Left = 0
    Top = 57
    Width = 759
    Height = 432
    Align = alClient
    BorderStyle = bsNone
    TabOrder = 1
    object Tela: TImage
      Left = 0
      Top = 0
      Width = 425
      Height = 305
    end
  end
end
