unit UFrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons;

const
  MARGEM_INICIAL = 10;

type
  TMazeCell = record
    Top: Boolean;
    Bottom: Boolean;
    Left: Boolean;
    Right: Boolean;
    Visited: Boolean;
  end;

type
  TMazeVector = Array of TMazeCell;
  
type
  TMaze = Array of TMazeVector;

type
  TMazeGridBoundary = record
    TopLeft: TPoint;
    TopRight: TPoint;
    BottomLeft: TPoint;
    BottomRight: TPoint;
  end;

type
  TMazeGridVerticalLines = record
    Top: TPoint;
    Bottom: TPoint;
  end;

type
  TMazeGridHorizontalLines = record
    Left: TPoint;
    Right: TPoint;
  end;

type
  TMazeGrid = record
    Boundary: TMazeGridBoundary;
    AxisXPoints: Array of TMazeGridVerticalLines;
    AxisYPoints: Array of TMazeGridHorizontalLines;
  end;

type
  IMazeGridDefiner = interface
  ['{449B837B-0E1E-4965-AC53-25DDB369196A}']
    function MazeGrid(AMaze: TMaze; ACellSize: Word): TMazeGrid;
  end;

type
  TMazeGridDefiner = class(TInterfacedObject, IMazeGridDefiner)
  public
    function MazeGrid(AMaze: TMaze; ACellSize: Word): TMazeGrid;
  end;

type
  TPathDirections = (pdUp, pdRight, pdDown, pdLeft);

type
  IMazePathOpener = interface
  ['{DF93917B-2C29-4758-8C40-24EB47DB4DDF}']
    procedure OpenPath(var AMaze: TMaze; ACelx, ACelY: Integer; ADirection: TPathDirections);
  end;

type
  TMazePathOpener = class(TInterfacedObject, IMazePathOpener)
  public
    procedure OpenPath(var AMaze: TMaze; ACelX, ACelY: Integer; ADirection: TPathDirections);
  end;

type
  IMazeCreator = interface
  ['{23159A42-163A-4D07-A31C-F3285C53E8D0}']
    procedure BuildMaze(var AMaze: TMaze);
  end;

type
  TBinaryTreeMazeCreator = class(TInterfacedObject, IMazeCreator)
  public
    procedure BuildMaze(var AMaze: TMaze);
  end;

type
  TSidewinderMazeCreator = class(TInterfacedObject, IMazeCreator)
  public
    procedure BuildMaze(var AMaze: TMaze);
  end;

type
  TRecursiveBacktrackingMazeCreator = class(TInterfacedObject, IMazeCreator)
  public
    procedure BuildMaze(var AMaze: TMaze);
  end;

type
  TForm3 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Button1: TButton;
    ScrollBox1: TScrollBox;
    Tela: TImage;
    rdgAlgoritmo: TRadioGroup;
    BitBtn1: TBitBtn;
    procedure Button1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    procedure MazeInitializate(var AMaze: TMaze);
    procedure MazePaint(ATela: TImage; AMaze: TMaze; AGrid: TMazeGrid);
    procedure PiscarCelula(ATela: TImage; ACorFundo, ACorPiscada: TColor; ATempoPiscada: Word; AGrid: TMazeGrid; ACelulaX, ACelulaY: Integer);
  public
    { Public declarations }
    LMaze: TMaze;
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

uses
  Generics.Collections, Printers;

procedure TForm3.BitBtn1Click(Sender: TObject);
var
  ScaleX, ScaleY: Integer;
  RR: TRect;
begin
  with Printer do
  begin
    BeginDoc;
    try
      ScaleX := GetDeviceCaps(Handle, logPixelsX) div PixelsPerInch;
      ScaleY := GetDeviceCaps(Handle, logPixelsY) div PixelsPerInch;
      RR := Rect(0, 0, Tela.picture.Width * scaleX, Tela.Picture.Height * ScaleY);
      Canvas.StretchDraw(RR, Tela.Picture.Graphic);
    finally
      EndDoc;
    end;
  end;
end;

procedure TForm3.Button1Click(Sender: TObject);
const
  TAMANHO_CELULA_LABIRINTO = 30;

var
//  LMaze: TMaze;
  I: Integer;

  QtdX: Integer;
  QtdY: Integer;

  LGridDefiner: IMazeGridDefiner;
  LGrid: TMazeGrid;

  LArchitect: IMazeCreator;
begin
  QtdX := StrToInt(Edit1.Text);
  QtdY := StrToInt(Edit2.Text);

  SetLength(LMaze, QtdX);
  for I := 0 to Length(LMaze) - 1 do
  begin
    SetLength(LMaze[I], QtdY);
  end;

  Tela.Picture := nil;

  MazeInitializate(LMaze);

  case rdgAlgoritmo.ItemIndex of
    0: LArchitect := TBinaryTreeMazeCreator.Create;
    1: LArchitect := TSidewinderMazeCreator.Create;
    2: LArchitect := TRecursiveBacktrackingMazeCreator.Create;
  end;
  
  LArchitect.BuildMaze(LMaze);

  LGridDefiner := TMazeGridDefiner.Create;
  LGrid := LGridDefiner.MazeGrid(LMaze, TAMANHO_CELULA_LABIRINTO);

  Tela.Height := (LGrid.Boundary.BottomLeft.Y - LGrid.Boundary.TopLeft.Y) + (2 * MARGEM_INICIAL);
  Tela.Width := (LGrid.Boundary.TopRight.X - LGrid.Boundary.TopLeft.X) + (2 * MARGEM_INICIAL);

  Tela.Canvas.Brush.Color := clWhite;

  MazePaint(Tela, LMaze, LGrid);
end;

{ TMazeCell }

procedure TForm3.MazeInitializate(var AMaze: TMaze);
var
  I: Integer;
  J: Integer;
begin
  if Length(AMaze) <= 0 then
  begin
    raise Exception.Create('O labirinto precisa ter alguma dimens�o!');
  end
  else
  begin
    for I := 0 to Length(AMaze) -1 do
    begin
      for J := 0 to Length(AMaze[0]) - 1 do
      begin
        AMaze[I, J].Top := True;
        AMaze[I, J].Bottom := True;
        AMaze[I, J].Left := True;
        AMaze[I, J].Right := True;
        AMaze[I, J].Visited := False;
      end;
    end;
  end;
end;

procedure TForm3.MazePaint(ATela: TImage; AMaze: TMaze; AGrid: TMazeGrid);
var
  LCanvas: TCanvas;
  I: Integer;
  J: Integer;
  LPonto: TPoint;


  
  LVetor: TMazeVector;
  LCelula: TMazeCell;

  PontoA: TPoint;
  PontoB: TPoint;
begin
  if Assigned(ATela) then
  begin
    LCanvas := ATela.Canvas;

    LCanvas.MoveTo(AGrid.Boundary.TopLeft.X, AGrid.Boundary.TopLeft.Y);
    LCanvas.LineTo(AGrid.Boundary.TopRight.X, AGrid.Boundary.TopRight.Y);
    LCanvas.LineTo(AGrid.Boundary.BottomRight.X, AGrid.Boundary.BottomRight.Y);
    LCanvas.LineTo(AGrid.Boundary.BottomLeft.X, AGrid.Boundary.BottomLeft.Y);
    LCanvas.LineTo(AGrid.Boundary.TopLeft.X, AGrid.Boundary.TopLeft.Y);


    {Pinta as linhas da grade}
//    for I := 0 to Length(AGrid.AxisXPoints) - 1 do
//    begin
//      LPonto := AGrid.AxisXPoints[I].Top;
//      LCanvas.MoveTo(LPonto.X, LPonto.Y);
//      LPonto := AGrid.AxisXPoints[I].Bottom;
//      LCanvas.LineTo(LPonto.X, LPonto.Y);
//    end;
//
//    for I := 0 to Length(AGrid.AxisYPoints) - 1 do
//    begin
//      LPonto := AGrid.AxisYPoints[I].Left;
//      LCanvas.MoveTo(LPonto.X, LPonto.Y);
//      LPonto := AGrid.AxisYPoints[I].Right;
//      LCanvas.LineTo(LPonto.X, LPonto.Y);
//    end;

    {Pinta as c�lulas da grade}
    for I := 0 to Length(AMaze) - 1 do
    begin
      for J := 0 to Length(AMaze[I]) - 1 do
      begin
        LCelula := AMaze[I][J];

        if LCelula.Top then
        begin
          PontoA.X := AGrid.AxisXPoints[I].Top.X;
          PontoA.Y := AGrid.AxisYPoints[J].Left.Y;

          PontoB.X := AGrid.AxisXPoints[I + 1].Top.X;
          PontoB.Y := AGrid.AxisYPoints[J].Left.Y;

          LCanvas.MoveTo(PontoA.X, PontoA.Y);
          LCanvas.LineTo(PontoB.X, PontoB.Y);
        end;

        if LCelula.Right then
        begin
          PontoA.X := AGrid.AxisXPoints[I + 1].Top.X;
          PontoA.Y := AGrid.AxisYPoints[J].Left.Y;

          PontoB.X := AGrid.AxisXPoints[I + 1].Top.X;
          PontoB.Y := AGrid.AxisYPoints[J + 1].Left.Y;

          LCanvas.MoveTo(PontoA.X, PontoA.Y);
          LCanvas.LineTo(PontoB.X, PontoB.Y);          
        end;

        if LCelula.Bottom then
        begin
          PontoA.X := AGrid.AxisXPoints[I + 1].Top.X;
          PontoA.Y := AGrid.AxisYPoints[J + 1].Left.Y;

          PontoB.X := AGrid.AxisXPoints[I].Top.X;
          PontoB.Y := AGrid.AxisYPoints[J + 1].Left.Y;

          LCanvas.MoveTo(PontoA.X, PontoA.Y);
          LCanvas.LineTo(PontoB.X, PontoB.Y);         
        end;

        if LCelula.Left then
        begin
          PontoA.X := AGrid.AxisXPoints[I].Top.X;
          PontoA.Y := AGrid.AxisYPoints[J].Left.Y;

          PontoB.X := AGrid.AxisXPoints[I].Top.X;
          PontoB.Y := AGrid.AxisYPoints[J + 1].Left.Y;

          LCanvas.MoveTo(PontoA.X, PontoA.Y);
          LCanvas.LineTo(PontoB.X, PontoB.Y);          
        end;
        
      end;
    end;
  end;
end;
                                                              
procedure TForm3.PiscarCelula(ATela: TImage; ACorFundo, ACorPiscada: TColor; ATempoPiscada: Word; AGrid: TMazeGrid; ACelulaX, ACelulaY: Integer);
var
  LPontoA: TPoint;
  LPontoB: TPoint;
begin
  if not Assigned(ATela) then
  begin
    raise Exception.Create('Voc� deve informar uma tela!');
  end
  else
  begin
    LPontoA.X := AGrid.AxisXPoints[ACelulaX].Top.X;
    LPontoA.Y := AGrid.AxisYPoints[ACelulaY].Left.Y;

    LPontoB.X := AGrid.AxisXPoints[ACelulaX + 1].Top.X + 1;
    LPontoB.Y := AGrid.AxisYPoints[ACelulaY + 1].Left.Y + 1;

    Tela.Canvas.Brush.Color := ACorPiscada;
    Tela.Canvas.Rectangle(LPontoA.X, LPontoA.Y, LPontoB.X, LPontoB.Y);
    Application.ProcessMessages;
    Sleep(ATempoPiscada);
    Tela.Canvas.Brush.Color := ACorFundo;
    Tela.Canvas.Rectangle(LPontoA.X, LPontoA.Y, LPontoB.X, LPontoB.Y);
  end;
end;

{ TScreenBoundaryDefiner }

function TMazeGridDefiner.MazeGrid(AMaze: TMaze; ACellSize: Word): TMazeGrid;

var
  I: Integer;
  PosX: Integer;
  PosY: Integer;
begin
  if ACellSize <= 0 then
  begin
    raise Exception.Create('O tamanho da c�lula precisa ser maior que zero!');
  end
  else  
  begin
    if Length(AMaze) <= 0 then
    begin
      raise Exception.Create('O labirinto informado n�o possui nenhuma dimens�o!');
    end
    else
    begin
      {##################################################
      #  IDENTIFICA PRIMEIRAMENTE OS LIMITES DA GRADE
      ##################################################}

      SetLength(Result.AxisXPoints, Length(AMaze) + 1);
      SetLength(Result.AxisYPoints, Length(AMaze[0]) + 1);

      Result.Boundary.TopLeft := TPoint.Create(MARGEM_INICIAL, MARGEM_INICIAL);

      PosX := MARGEM_INICIAL;

      for I := 1 to Length(AMaze) do
      begin
        PosX := PosX + ACellSize + 1; //1 = tamanho em pixel da linha pintada
      end;

      Result.Boundary.TopRight := TPoint.Create(PosX, MARGEM_INICIAL);

      PosY := MARGEM_INICIAL;

      for I := 1 to Length(AMaze[0]) do
      begin
        PosY := PosY + ACellSize + 1;
      end;

      Result.Boundary.BottomLeft := TPoint.Create(MARGEM_INICIAL, PosY);
      Result.Boundary.BottomRight := TPoint.Create(PosX, PosY);

      {#####################################################
      #  IDENTIFICA O PPOSICIONAMENTO DAS LINHAS DO EIXO X
      #####################################################}
      SetLength(Result.AxisXPoints, Length(AMaze) + 1);

      PosX := Result.Boundary.TopLeft.X;

      for I := 0 to Length(AMaze) do
      begin
        Result.AxisXPoints[I].Top := TPoint.Create(PosX, Result.Boundary.TopLeft.Y);
        Result.AxisXPoints[I].Bottom := TPoint.Create(PosX, Result.Boundary.BottomLeft.Y);

        PosX := PosX + ACellSize + 1;
      end;

      {#####################################################
      #  IDENTIFICA O PPOSICIONAMENTO DAS LINHAS DO EIXO Y
      #####################################################}
      SetLength(Result.AxisYPoints, Length(AMaze[0]) + 1);

      PosY := Result.Boundary.TopLeft.Y;

      for I := 0 to Length(AMaze[0]) do
      begin
        Result.AxisYPoints[I].Left := TPoint.Create(Result.Boundary.TopLeft.X, PosY);
        Result.AxisYPoints[I].Right := TPoint.Create(Result.Boundary.TopRight.X, PosY);

        PosY := PosY + ACellSize + 1;
      end;
    end;
  end;
end;

{ TBinaryTreeMazeCreator }

procedure TBinaryTreeMazeCreator.BuildMaze(var AMaze: TMaze);
var
  I: Integer;
  J: Integer;
  LMiter: IMazePathOpener;
begin
  if Length(AMaze) = 0 then
  begin
    raise Exception.Create('O labirinto precisa possuir alguma dimens�o.');
  end
  else
  begin
    LMiter := TMazePathOpener.Create;

    Randomize;

    for I := 0 to Length(AMaze) - 1 do
    begin
      for J := 0 to Length(AMaze[0]) - 1 do
      begin
        if J = 0 then
        begin
          LMiter.OpenPath(AMaze, I, J, pdRight);
        end
        else
        begin
          if I = Length(AMaze) - 1 then
          begin
            LMiter.OpenPath(AMaze, I, J, pdUp);
          end
          else
          begin
            if Random(2) = 1 then
            begin
              LMiter.OpenPath(AMaze, I, J, pdUp);
            end
            else
            begin
              LMiter.OpenPath(AMaze, I, J, pdRight);
            end;
          end;
        end;
      end;
    end;
  end;
end;

{ TMazePathOpener }

procedure TMazePathOpener.OpenPath(var AMaze: TMaze; ACelX, ACelY: Integer; ADirection: TPathDirections);
begin
  case ADirection of
       pdUp: begin
               if ACelY > 0 then
               begin
                 AMaze[ACelX][ACelY].Top := False;
                 AMaze[ACelX][ACelY - 1].Bottom := False;
               end;
             end;
    pdRight: begin
               if ACelX < (Length(AMaze) - 1) then
               begin
                 AMaze[ACelX][ACelY].Right := False;
                 AMaze[ACelX + 1][ACelY].Left := False;
               end;
             end;
    pdDown: begin
               if ACelY < (Length(AMaze[0]) - 1) then
               begin
                 AMaze[ACelX][ACelY].Bottom := False;
                 AMaze[ACelX][ACelY + 1].Top := False;
               end;
            end;
    pdLeft: begin
               if ACelX > 0 then
               begin
                 AMaze[ACelX][ACelY].Left := False;
                 AMaze[ACelX - 1][ACelY].Right := False;
               end;
            end;
  end;
end;

{ TSidewinderCreator }

procedure TSidewinderMazeCreator.BuildMaze(var AMaze: TMaze);
var
  I: Integer;
  J: Integer;
  LMiter: IMazePathOpener;

  procedure WorkRunSet(var PMaze: TMaze; ALineNumber: Integer; AInitialPosition: Integer);
  var
    LFinalPosition: Integer;
    X: Integer;
    LPositionToCaveUp: Integer;
    LQtdAvailableCells: Integer;
    LQtdDrawn: Integer;
    LExistOnlyOneCell: Boolean;
    LIsTheLastCell: Boolean;
    QtdInsideRunSet: Integer;
  begin
    LQtdAvailableCells := (Length(AMaze)) - AInitialPosition;

    //Percebi que se deixar isso livre, quando o labirinto �
    //muito grande, o layoit final dele fica feio e f�cil de solucionar.
    //Por isso, estou limitando a apenas 4 c�lulas, no m�ximo.
    if LQtdAvailableCells > 4 then
    begin
      LQtdDrawn := Random(4);
    end
    else
    begin
      LQtdDrawn := Random(LQtdAvailableCells);
    end;

    LFinalPosition := AInitialPosition + LQtDdrawn;


    LExistOnlyOneCell := LFinalPosition = AInitialPosition;
    
    if LExistOnlyOneCell then
    begin
      LMiter.OpenPath(PMaze, AInitialPosition, J, pdUp);

      LIsTheLastCell := LFinalPosition >= Length(PMaze) - 1;
      
      if not LIsTheLastCell then
      begin
        WorkRunSet(PMaze, ALineNumber, LFinalPosition + 1);
      end;
    end
    else
    begin
      for X := AInitialPosition to LFinalPosition - 1 do
      begin
        LMiter.OpenPath(PMaze, X, ALineNumber, pdRight);
      end;

      QtdInsideRunSet := LFinalPosition - AInitialPosition + 1;
      LPositionToCaveUp := AInitialPosition + Random(QtdInsideRunSet);
      LMiter.OpenPath(PMaze, LPositionToCaveUp, ALineNumber, pdUp);

      LIsTheLastCell := LFinalPosition >= Length(PMaze) - 1;
      
      if not LIsTheLastCell then
      begin
        WorkRunSet(PMaze, ALineNumber, LFinalPosition + 1);
      end;
    end;
  end;
begin
  if Length(AMaze) = 0 then
  begin
    raise Exception.Create('O labirinto precisa possuir alguma dimens�o.');
  end
  else
  begin
    LMiter := TMazePathOpener.Create;

    Randomize;

    for J := 0 to Length(AMaze[0]) - 1 do
    begin
      if J = 0 then
      begin
        for I := 0 to Length(AMaze) - 1 do
        begin
          LMiter.OpenPath(AMaze, I, J, pdRight);
        end;
      end
      else
      begin
        WorkRunSet(AMaze, J, 0);
      end;
    end;
  end;
end;

{ TRecursiveBacktrackingMazeCreator }

procedure TRecursiveBacktrackingMazeCreator.BuildMaze(var AMaze: TMaze);
var
  LChoosenStartX: Integer;
  LChoosenStartY: Integer;
  LMiter: IMazePathOpener;

  function ValidPath(var PMaze: TMaze; ACellX, ACellY: Integer; AExpectedDirection: TPathDirections): Boolean;
  begin
    case AExpectedDirection of
      pdUp   : begin
                 Result := ACellY > 0;
                 if Result then
                 begin
                   Result := not PMaze[ACellX, ACellY - 1].Visited;
                 end;
               end;
      pdRight: begin
                 Result := ACellX < Length(PMaze) - 1;
                 if Result then
                 begin
                   Result := not PMaze[ACellX + 1, ACellY].Visited;
                 end;
               end;
      pdDown : begin
                 Result := ACellY < Length(PMaze[0]) - 1;
                 if Result then
                 begin
                   Result := not PMaze[ACellX, ACellY + 1].Visited;
                 end;
               end;
      pdLeft : begin
                 Result := ACellX > 0;
                 if Result then
                 begin
                   Result := not PMaze[ACellX - 1, ACellY].Visited;
                 end;
               end;
    end;
  end;

  procedure WalkThrough(var PMaze: TMaze; ANewCellX, ANewCellY: Integer);
  var
    LListOfPossiblePaths: TList<TPathDirections>;
    LDirection: Word;
    I: Integer;
  begin
    PMaze[ANewCellX, ANewCellY].Visited := True;

    LListOfPossiblePaths := TList<TPathDirections>.Create;
    try
      LListOfPossiblePaths.Clear;

      for I := Ord(Low(TPathDirections)) to Ord(High(TPathDirections)) do
      begin
        LListOfPossiblePaths.Add(TPathDirections(I));
      end;

      while LListOfPossiblePaths.Count > 0 do
      begin
        LDirection := Random(LListOfPossiblePaths.Count);

        if not ValidPath(PMaze, ANewCellX, ANewCellY, LListOfPossiblePaths.Items[LDirection]) then
        begin
          LListOfPossiblePaths.Delete(LDirection);
        end
        else
        begin
          LMiter.OpenPath(PMaze, ANewCellX, ANewCellY, LListOfPossiblePaths.Items[LDirection]);

          case LListOfPossiblePaths.Items[LDirection] of
            pdUp   : WalkThrough(PMaze, ANewCellX, ANewCellY - 1);
            pdRight: WalkThrough(PMaze, ANewCellX + 1, ANewCellY);
            pdDown : WalkThrough(PMaze, ANewCellX, ANewCellY + 1);
            pdLeft : WalkThrough(PMaze, ANewCellX - 1, ANewCellY);
          end;
        end;
      end;
    finally
      LListOfPossiblePaths.Free;
    end;
  end;
begin
  Randomize;
  LChoosenStartX := Random(Length(AMaze));
  LChoosenStartY := Random(Length(AMaze[0]));

  LMiter := TMazePathOpener.Create;

  WalkThrough(AMaze, LChoosenStartX, LChoosenStartY);
end;

end.
