object frmPrincipal: TfrmPrincipal
  Left = 0
  Top = 0
  Caption = 'Formul'#225'rio'
  ClientHeight = 154
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 16
    Width = 80
    Height = 13
    Caption = 'N'#237'vel do usu'#225'rio:'
  end
  object Button1: TButton
    Left = 32
    Top = 88
    Width = 178
    Height = 25
    Caption = 'Primeira forma de Impress'#227'o'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 232
    Top = 88
    Width = 169
    Height = 25
    Caption = 'Segunda forma de Impress'#227'o'
    TabOrder = 2
    OnClick = Button2Click
  end
  object spnNilvelAcessoUsuario: TSpinEdit
    Left = 32
    Top = 35
    Width = 80
    Height = 22
    MaxValue = 3
    MinValue = 1
    TabOrder = 0
    Value = 1
    OnChange = spnNilvelAcessoUsuarioChange
  end
end
