unit UFrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Rtti, Vcl.Samples.Spin;

type
  {$M+}
  TUsuario = class
  private
    FLogin: string;
    FNivelAcesso: Word;
  published
    property Login: string read FLogin write FLogin;
    property NivelAcesso: Word read FNivelAcesso write FNivelAcesso;
  end;

  TLog = class
  private
    const CAMINHO_ARQUIVO = 'Log.txt';
  public
    procedure Loggar(AUsuario: TUsuario; ATexto: String);
  end;

type
  TValidadorSeguranca = class
  public
    function AcessoPermitido(AUsuario: TUsuario; ANivelSegurancaNecessario: Integer): Boolean;
  end;

const
  NIVEL_ACESSO_NECESSARIO_PARA_ACESSAR_A_FUNCIONALIDADE_DE_IMPRESSAO = 2;

type
  TImpressoraTela = class
  public
    procedure Imprimir(AUsuario: TUsuario; ATexto: String); overload;
    procedure Imprimir(ATexto: String); overload; virtual;
  end;

type
  TfrmPrincipal = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    spnNilvelAcessoUsuario: TSpinEdit;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure spnNilvelAcessoUsuarioChange(Sender: TObject);
  private
    { Private declarations }
    FProxy: TVirtualMethodInterceptor;
    FImpressora: TImpressoraTela;
    FUsuario: TUsuario;
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

{ TLog }

procedure TLog.Loggar(AUsuario: TUsuario; ATexto: String);
var
  Arquivo: TFileStream;
  TextoNoFormatoAdequadoParaGravacao: TBytes;
begin
  if FileExists(CAMINHO_ARQUIVO) then
  begin
    Arquivo := TFileStream.Create(CAMINHO_ARQUIVO, fmOpenReadWrite);
  end
  else
  begin
    Arquivo := TFileStream.Create(CAMINHO_ARQUIVO, fmCreate);
  end;
  try
    TextoNoFormatoAdequadoParaGravacao := TEncoding.UTF8.GetBytes(ATexto);

    Arquivo.Write(TextoNoFormatoAdequadoParaGravacao, Length(TextoNoFormatoAdequadoParaGravacao));
  finally
    FreeAndNil(Arquivo);
  end;
end;

{ TValidacaoSeguranca }

function TValidadorSeguranca.AcessoPermitido(AUsuario: TUsuario; ANivelSegurancaNecessario: Integer): Boolean;
begin
  Result := AUsuario.FNivelAcesso >= ANivelSegurancaNecessario;
end;

{ TImpressoraTela }

procedure TImpressoraTela.Imprimir(AUsuario: TUsuario; ATexto: String);
var
  Log: TLog;
  Acesso: TValidadorSeguranca;
begin
  Log := Tlog.Create;
  try
    Acesso := TValidadorSeguranca.Create;
    try
      if Acesso.AcessoPermitido(AUsuario, NIVEL_ACESSO_NECESSARIO_PARA_ACESSAR_A_FUNCIONALIDADE_DE_IMPRESSAO) then
      begin
        ShowMessage(ATexto);
        Log.Loggar(AUsuario, ATexto);
      end
      else
      begin
        Log.Loggar(AUsuario, 'Tentativa de acesso negada a Imprimir!');
        raise Exception.Create('Voc� n�o pode acessar essa funcionalidade porque o seu n�vel de acesso n�o permite!');
      end;
    finally
      FreeAndNil(Acesso);
    end;
  finally
    FreeAndNil(Log);
  end;
end;

procedure TImpressoraTela.Imprimir(ATexto: String);
begin
  ShowMessage(ATexto);
end;

procedure TfrmPrincipal.Button1Click(Sender: TObject);
begin
  FImpressora.Imprimir(FUsuario, 'Primeira forma de Impress�o!');
end;

procedure TfrmPrincipal.Button2Click(Sender: TObject);
begin
  FImpressora.Imprimir('Segunda forma de Impress�o');
end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin
  {Simula��o do carregamento do usu�rio. Em um ambiente real, isso � bem mais elegante}
  FUsuario := TUsuario.Create;
  FUsuario.FLogin := 'Edgar';
  FUsuario.FNivelAcesso := 1;

  FImpressora := TImpressoraTela.Create;

  FProxy := TVirtualMethodInterceptor.Create(FImpressora.ClassType);

  FProxy.OnBefore := procedure(Instance: TObject; Method: TRttiMethod; const Args: TArray<TValue>; out DoInvoke: Boolean; out Result: TValue)
                     var
                       ValidadorSeguranca: TValidadorSeguranca;
                       Log: TLog;
                     begin
                       if Method.Name = 'Imprimir' then
                       begin
                         ValidadorSeguranca := TValidadorSeguranca.Create;
                         try
                           if not ValidadorSeguranca.AcessoPermitido(FUsuario, NIVEL_ACESSO_NECESSARIO_PARA_ACESSAR_A_FUNCIONALIDADE_DE_IMPRESSAO) then
                           begin
                             Log := TLog.Create;
                             try
                               Log.Loggar(FUsuario, 'Tentativa de acesso � impress�o negada por falta de acesso.');
                             finally
                               Log.Free;
                             end;

                             DoInvoke := False;

                             raise Exception.Create('Voc� n�o pode acessar essa funcionalidade porque o seu n�vel de acesso n�o permite!');
                           end;
                         finally
                           ValidadorSeguranca.Free;
                         end;
                       end;
                     end;

  FProxy.OnAfter := procedure(Instance: TObject; Method: TRttiMethod; const Args: TArray<TValue>; var Result: TValue)
                    var
                      Log: TLog;
                    begin
                      if Method.Name = 'Imprimir' then
                      begin
                        Log := TLog.Create;
                        try
                          Log.Loggar(FUsuario, 'Acesso permitido � impress�o.');
                        finally
                          Log.Free;
                        end;
                      end;
                    end;

  FProxy.Proxify(FImpressora);
end;

procedure TfrmPrincipal.FormDestroy(Sender: TObject);
begin
  if Assigned(FImpressora) then FImpressora.Free;
  if Assigned(FProxy) then FProxy.Free;
  if Assigned(FUsuario) then FUsuario.Free;
end;

procedure TfrmPrincipal.spnNilvelAcessoUsuarioChange(Sender: TObject);
begin
  FUsuario.NivelAcesso := spnNilvelAcessoUsuario.Value;
end;

end.

