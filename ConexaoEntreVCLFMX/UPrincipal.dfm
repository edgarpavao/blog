object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Principal'
  ClientHeight = 305
  ClientWidth = 544
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 8
    Top = 8
    Width = 528
    Height = 249
    DataSource = srcQuery
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Edit1: TEdit
    Left = 8
    Top = 272
    Width = 409
    Height = 21
    Enabled = False
    TabOrder = 1
  end
  object Button1: TButton
    Left = 463
    Top = 270
    Width = 75
    Height = 25
    Caption = 'Carregar DLL'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 418
    Top = 272
    Width = 24
    Height = 21
    Caption = '...'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Query: TFDQuery
    Active = True
    Connection = ModuloDados.Conexao
    SQL.Strings = (
      'SELECT * FROM TABELA')
    Left = 168
    Top = 56
  end
  object srcQuery: TDataSource
    DataSet = Query
    Left = 200
    Top = 56
  end
  object OpenDialog1: TOpenDialog
    Left = 272
    Top = 168
  end
end
