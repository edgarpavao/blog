program VCLProject;

uses
  Vcl.Forms,
  UPrincipal in 'UPrincipal.pas' {Form3},
  UDataModule in 'UDataModule.pas' {ModuloDados: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TModuloDados, ModuloDados);
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
