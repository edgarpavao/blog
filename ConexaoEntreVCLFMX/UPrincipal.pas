unit UPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls;

type
  TForm3 = class(TForm)
    Query: TFDQuery;
    srcQuery: TDataSource;
    DBGrid1: TDBGrid;
    Edit1: TEdit;
    Button1: TButton;
    OpenDialog1: TOpenDialog;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

uses UDataModule;

procedure TForm3.Button1Click(Sender: TObject);
var
  LThread: TThread;
  LHandle: Cardinal;
  LCarrgearFormularioFMX: procedure(AHandle: Pointer);
begin
  if Trim(Edit1.Text) = EmptyStr  then
  begin
    ShowMessage('Voc� deve informar uma dll!');
  end
  else
  begin
    LThread := TThread.CreateAnonymousThread(procedure
    begin
      LHandle := LoadLibrary(PChar(Edit1.Text));

      if LHandle <> 0 then
      begin
        @LCarrgearFormularioFMX := GetProcAddress(LHandle, 'CarregarFormulario');

        if Assigned(LCarrgearFormularioFMX) then
        begin
          LCarrgearFormularioFMX(ModuloDados.Conexao.CliHandle);
        end
        else
        begin
          ShowMessage('Problemas ao carregar o formul�rio!');
        end;

        FreeLibraryAndExitThread(LHandle, 0);
      end;
    end);
    LThread.Start;
  end;
end;

procedure TForm3.Button2Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    Edit1.Text := OpenDialog1.FileName;
  end;
end;

end.
